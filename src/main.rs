use evdev_rs::{enums::*, *};
use glob::glob;
use libc;
use nix::poll::{poll, EventFlags, PollFd};
use serde::Deserialize;
use std::collections::HashMap;
use std::fs::{metadata, Metadata, read_to_string, File};
use std::os::unix::{
    fs::{MetadataExt, PermissionsExt},
    io::AsRawFd,
};
use std::process::Command;
use std::string::String;
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread;
use std::vec::Vec;
use toml;

static VERBOSE: AtomicBool = AtomicBool::new(false);
static ALLOW_INSECURE_CONFIG: AtomicBool = AtomicBool::new(false);

#[derive(Debug, Deserialize, Copy, Clone, PartialEq)]
#[serde(rename_all = "lowercase")]
enum KeyState {
    Released,
    Pressed,
    Held,
}

impl std::str::FromStr for KeyState {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "released" => Ok(KeyState::Released),
            "pressed" => Ok(KeyState::Pressed),
            "held" => Ok(KeyState::Held),
            _ => Err(()),
        }
    }
}

impl KeyState {
    fn from_int(i: i32) -> Self {
        match i {
            0 => KeyState::Released,
            1 => KeyState::Pressed,
            2 => KeyState::Held,
            _ => panic!("Invalid key state: {}", i),
        }
    }

    fn to_int(&self) -> i32 {
        match self {
            KeyState::Released => 0,
            KeyState::Pressed => 1,
            KeyState::Held => 2,
        }
    }
}

/*
 * A preface on state management:
 * The hashmap tracked_keys is used to track the state of all they keys
 * which we care about. It is a hashmap of keycode (EV_KEY) -> KeyStateLifetime.
 * KeyStateLifetime tracks the state of the key and when the state last changed (valid).
 * Valid is true until the key is "processed", this effectively means of the set of keys,
 * at most one key will be valid at any given time, that key should be the key that is
 * being pressed.
 * This ensures that we don't process multiple events for one press (e.g)
 * if you have an event mapped to LEFTCTRL and another mapped to LEFTCTRL + B, if you hold
 * LEFTCTRL and press B, you would expect the LEFTCTRL + B event to be processed, but not
 * the LEFTCTRL event (as it should only be processed at the time that key is pressed, not
 * when a different key is pressed).
 */
#[derive(Debug, Deserialize)]
struct KeyStateLifetime {
    state: KeyState,
    valid: bool,
}

#[derive(Deserialize, Debug)]
struct Entry {
    name: Option<String>,
    event_type: EventType,
    keys: Option<Vec<EV_KEY>>,
    key_state: Option<KeyState>,
    command: String,
}

#[derive(Deserialize, Debug)]
struct Config {
    path: Option<String>,
    events: Vec<Entry>,
}

/// State 😎
struct HKDM {
    configs: Vec<Config>,
    devices: Vec<Device>,
    tracked_keys: HashMap<EV_KEY, KeyStateLifetime>,
}

// Shamelessly stolen from https://github.com/Vurich/sexpress/blob/master/src/lib.rs#L79-L103
macro_rules! debug {
    (@preamble) => {{
        if (VERBOSE.load(Ordering::Relaxed)) {
            use std::sync::atomic::{AtomicUsize, Ordering};
            static DEBUG: AtomicUsize = AtomicUsize::new(0);
            print!(
                "{:04} {}:{},{}",
                DEBUG.fetch_add(1, Ordering::Relaxed),
                file!(),
                line!(),
                column!(),
            );
        }
    }};
    () => {
        if (VERBOSE.load(Ordering::Relaxed)) {
            debug!(@preamble);
            println!();
        }
    };
    ($first:expr $(, $rest:expr)* $(,)*) => {
        if (VERBOSE.load(Ordering::Relaxed)) {
            debug!(@preamble);
            //print!("{} = {:?}", stringify!($first), $first);
            $(print!(": {} = {:?}", stringify!($rest), $rest);)*
            println!();
        }
    };
}

impl HKDM {
    fn update_tracked_keys(&mut self, ev: &InputEvent) {
        // Update the tracked_keys map
        let evkey = match ev.event_code {
            EventCode::EV_KEY(code) => code,
            _ => return,
        };

        /*
         * Make sure all keys are marked invalid to avoid them being processed
         * in match_event.
         */
        self.tracked_keys.values_mut().for_each(|k| k.valid = false);

        if let Some(tk) = self.tracked_keys.get_mut(&evkey) {
            *tk = KeyStateLifetime {
                state: KeyState::from_int(ev.value),
                valid: true,
            };
        }
        debug!("{:?}, {:?}", evkey, self.tracked_keys.get(&evkey).unwrap());
    }
}

/// Returns true if the input event matches the config entry.
fn event_match(
    config_entry: &Entry,
    tracked_keys: &HashMap<EV_KEY, KeyStateLifetime>,
    ev: &InputEvent,
) -> bool {
    let mut match_counter = 0;
    let keys = config_entry.keys.as_ref().unwrap();
    let keys_len = keys.len();
    let evkey = match ev.event_code {
        EventCode::EV_KEY(code) => code,
        _ => return false,
    };
    // Avoid processing events that don't reference the event in &ev
    if !keys.contains(&evkey) {
        return false;
    }
    debug!("{:?}", tracked_keys);
    for i in 0..keys_len {
        let ks = match config_entry.key_state {
            Some(k) => k.to_int(),
            None => KeyState::Released.to_int(),
        };
        if let Some(key_state) = tracked_keys.get(&keys[i]) {
            let keycode = keys[i];
            //debug!("{:?}: {:?}, {:?}, {:?}, {:?}, {:?}", i, config_entry.key_state, key_state, ev.value, ev.event_code, keycode);
            if keys_len > 1 {
                if (key_state.state == KeyState::Pressed || key_state.state == KeyState::Held)
                        && keycode != evkey {
                    match_counter += 1;
                } else if i == keys_len - 1 && keycode == evkey && ev.value == ks {
                    match_counter += 1;
                }
            } else {
                if keycode == evkey && ev.value == ks {
                    match_counter += 1;
                }
            }
        }
    }

    return match_counter == keys_len;
}


/// Run given command in a new thread
fn run_cmd(cmd: String) {
    thread::spawn(move || {
        Command::new("sh")
            .arg("-c")
            .arg(cmd.as_str())
            .output()
            .unwrap_or_else(|_| panic!("Failed to run command: {}", cmd));
    });
}

/// Handler for input events.
fn handle_event(m: &HKDM, ev: &InputEvent) {
    for config in &m.configs {
        for entry in &config.events {
            if event_match(&entry, &m.tracked_keys, &ev) {
                debug!("{:?}", entry);
                run_cmd(entry.command.clone());
            }
        }
    }
}

/// Main loop, iterates over input devices
/// and handles events.
fn event_loop(m: &mut HKDM) {
    let mut a: std::io::Result<(ReadStatus, InputEvent)>;
    let mut fds_arr = m
        .devices
        .iter()
        .map(|d| PollFd::new(d.file().as_raw_fd(), EventFlags::POLLIN))
        .collect::<Vec<_>>();
    loop {
        match poll(&mut fds_arr, 5000) {
            Err(_) => continue, // No events to read so loop again
            Ok(_) => {}
        };
        for i in 0..m.devices.len() {
            let device = &mut m.devices[i];
            // From docs, we might need a smart implementation here at some point.
            if !device.has_event_pending() {
                continue;
            }
            a = device.next_event(ReadFlag::NORMAL);
            if a.is_ok() {
                let result = a.ok().unwrap();
                match result.0 {
                    ReadStatus::Sync => {
                        a = device.next_event(ReadFlag::SYNC);
                        if !a.is_ok() {
                            continue;
                        }
                    }
                    ReadStatus::Success => {}
                }
                let event = result.1;
                if event.event_type() == Some(EventType::EV_KEY) {
                    let evkey = match event.event_code {
                        EventCode::EV_KEY(code) => code,
                        _ => continue,
                    };
                    if m.tracked_keys.contains_key(&evkey) {
                        m.update_tracked_keys(&event);
                        handle_event(&m, &event);
                    }
                }
            } else {
                let err = a.err().unwrap();
                match err.raw_os_error() {
                    Some(libc::EAGAIN) => continue,
                    _ => {
                        debug!("{}", err);
                        break;
                    }
                }
            }
        }
    }
}

/// Parses the config to get a list of all keys
/// for which we need to track state.
fn get_all_keys(confg: &Vec<Config>) -> HashMap<EV_KEY, KeyStateLifetime> {
    let mut tracked_keys: HashMap<EV_KEY, KeyStateLifetime> = HashMap::new();
    for config in confg {
        for event in &config.events {
            if let Some(ref keys) = event.keys {
                for key in keys {
                    tracked_keys.insert(
                        *key,
                        KeyStateLifetime {
                            state: KeyState::Released,
                            valid: false,
                        },
                    );
                }
            }
        }
    }
    return tracked_keys;
}

fn parse_args() -> String {
    let mut configs_path: String = "/etc/hkdm/config.d".to_string();
    let args_iter: Vec<String> = std::env::args().collect();

    for i in 0..args_iter.len() {
        let arg = &args_iter[i];
        if arg == "-v" || arg == "--verbose" {
            VERBOSE.store(true, Ordering::Relaxed);
        }
        if (arg == "-c" || arg == "--config") && i + 1 < args_iter.len() {
            configs_path = args_iter[i + 1].clone();
        }
        if arg == "-i" || arg == "--insecure" {
            ALLOW_INSECURE_CONFIG.store(true, Ordering::Relaxed);
        }
        if arg == "-h" || arg == "--help" {
            println!("HKDM - A headless hotkey daemon");
            println!("Usage: hkdm [ -v|--verbose ] [ -c|--config CONFIG_DIR ] [ -i|--insecure ]");
            println!("--insecure: bypass permissions checking on config files, allowing the use
of config files which are writable by non-root users,
this may allow malicious users to log keypresses!");
            std::process::exit(1);
        }
    }

    configs_path
}

fn load_devices() -> Vec<Device> {
    let mut devices = Vec::new();
    for entry in glob("/dev/input/event*").unwrap() {
        match entry {
            Ok(path) => {
                debug!("{:?}", path);
                let file = File::open(path).unwrap();
                let device = Device::new_from_file(file).unwrap();
                devices.push(device);
            }
            Err(e) => println!("{:?}", e),
        }
    }
    return devices;
}

/// Returns true if the given path passes
/// the permissions check.
/// it must be only writable by root.
fn check_perms(md: &Metadata) -> bool {
    if ALLOW_INSECURE_CONFIG.load(Ordering::Relaxed) {
        return true;
    }
    let perms = md.permissions();
    debug!("{:?}, {:?}", perms.mode(), md.gid());
    perms.mode() & 0o002 == 0 && md.gid() == 0
}

fn parse_configs(configs_path: String) -> Result<Vec<Config>, &'static str> {
    let mut configs = Vec::new();
    let mut config_path = configs_path.clone();
    if config_path.ends_with('/') {
        config_path.pop();
    }

    match metadata(config_path.as_str()) {
        Ok(md) => {
            if !check_perms(&md) {
                return Err("ERROR: Config directory writable by non-root user
this is a potential security risk as a malicious
config file could be used to log keypresses.");
            }
            if md.is_dir() {
                config_path.push_str("/*");
            }
        }
        Err(_) => {
            return Err("Config directory does not exist");
        }
    };

    debug!("{:?}", config_path);
    let glob_result = match glob(&config_path) {
        Ok(g) => g,
        Err(e) => {
            println!("Couldn't to parse pattern '{:?}': {:?}", config_path, e);
            return Err("Failed to parse pattern");
        }
    };

    for entry in glob_result {
        if let Ok(entry) = entry {
            debug!("{:?}", entry);
            if ! str::ends_with(entry.to_str().unwrap(), ".toml") {
                continue;
            }
            match metadata(&entry) {
                Ok(md) => {
                    if !check_perms(&md) {
                        return Err("ERROR: Config file writable by non-root user
        this is a potential security risk as a malicious
        config file could be used to log keypresses.");
                    }
                }
                Err(_) => {
                    return Err("Config directory does not exist");
                }
            };
            let config = toml::from_str::<Config>(read_to_string(&entry).unwrap().as_mut_str());
            match config {
                Ok(c) => configs.push(c),
                Err(e) => eprintln!("Error parsing {:?}, {}", entry, e),
            }
        }
    }

    match configs.len() {
        0 => Err("No configs loaded"),
        _ => Ok(configs),
    }
}

fn main() {
    let configs_path = parse_args();
    let configs = match parse_configs(configs_path) {
        Ok(c) => c,
        Err(e) => {
            eprintln!("Failed to parse configs: {}", e.to_string());
            std::process::exit(2);
        }
    };
    let tracked_keys = get_all_keys(&configs);
    let devices = load_devices();
    let mut hkdm = HKDM {
        devices: devices,
        configs: configs,
        tracked_keys: tracked_keys,
    };
    event_loop(&mut hkdm);
}
